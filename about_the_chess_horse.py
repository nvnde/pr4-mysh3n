from collections import deque
import argparse
from graph import graph


def knight_move(start, finish):
    def btf(start_help, finish_help, graph_help):
        queue = deque([start_help])
        visited_help = {start_help: None}

        while queue:
            cur_node_help = queue.popleft()
            if cur_node_help == finish_help:
                break

            next_nodes = graph_help[cur_node_help]
            for next_node in next_nodes:
                if next_node not in visited_help:
                    queue.append(next_node)
                    visited_help[next_node] = cur_node_help
        return visited_help

    visited = btf(start, finish, graph)
    cur_node = finish
    cnt = 0
    while cur_node != start:
        cur_node = visited[cur_node]
        cnt += 1
    return cnt


def knights_collision(first, second):
    if knight_move(first, second) % 2 == 0:
        return knight_move(first, second) // 2
    return knight_move(first, second) // 2 + 1


parser = argparse.ArgumentParser()
parser.add_argument("start_or_first", type=str)
parser.add_argument("finish_or_second", type=str)
parser.add_argument("func", type=str)
args = parser.parse_args()
if args.start_or_first.isdigit() and args.finish_or_second.isdigit():
    if 11 <= int(args.start_or_first) <= 88 and 11 <= int(args.finish_or_second) <= 88:
        if args.func == 'move':
            start_cmd = (int(args.start_or_first[0]), int(args.start_or_first[1]))
            finish_cmd = (int(args.finish_or_second[0]), int(args.finish_or_second[1]))
            print(f'Необходимое количество ходов: {knight_move(start_cmd, finish_cmd)}')
        elif args.func == 'collision':
            first_cmd = (int(args.start_or_first[0]), int(args.start_or_first[1]))
            second_cmd = (int(args.finish_or_second[0]), int(args.finish_or_second[1]))
            print(f'Минимальное количество ходов для встречи: {knights_collision(first_cmd, second_cmd)}\n'
                  f'Один ход - ход сразу двух коней, но на последнем ходу один конь может остаться на месте')
        else:
            print('Вы что-то делаете не так!')
    else:
        print('Вы что-то делаете не так!')
else:
    print('Вы что-то делаете не так!')
